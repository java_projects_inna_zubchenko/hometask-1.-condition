package com.epam.test.automation.java.practice1;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MainTest {

    @Test()
    public void test1(){
        Assert.assertEquals(Main.task1(3), 9, "The method task1 works incorrectly");
    }

    @Test(priority=1)
    public void test2(){
        Assert.assertEquals(Main.task2(327), 732, "The method task2 works incorrectly");
    }
}